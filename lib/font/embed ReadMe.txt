*** embed - http://carnage-melon.tom7.org/embed/ ***
This is a quick program I made, according to Microsoft's TrueType Specifications (http://www.microsoft.com/typography/tt/tt.htm), which will change the embedding level of a TrueType font. I made this for myself, since I noticed that a lot of the Fonts I Made (http://fonts.tom7.com/) had their embedding set in a very restricting manner. Microsoft's font properties editor (http://www.microsoft.com/typography/property/fpedit.htm) does not let you lower this setting (though you can make the license more restrictive).

This program will quickly and automatically set the font to 'installable embedding allowed', the least restrictive setting. You can use Microsoft's editor to make it a more restrictive value, or change it in the source code, if you really require a different embedding level.

To run it, simply give it the name of the ttf file on the command line:

embed font.ttf

No error messages signifies success.

embed.exe [ 23k ] (http://carnage-melon.tom7.org/embed/embed.exe) - This is the executable, and all you need (if you're not running Windows 95/98/NT, you'll need a DOS Extender, actually)

embed.c [ 2k ] (http://carnage-melon.tom7.org/embed/embed.c) - Source code in C, if you want to play around with it or compile for a non-windows platform.

Both the program and source are in the public domain. Enjoy.

NOTE: Changing the embedding value does not give you license to distribute the fonts. You should only change this setting if you are the font creator, or something like that. Use at your own risk.

